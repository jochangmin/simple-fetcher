'''
Created on 2014. 6. 20.

@author: a141890
'''
from PIL import Image
from Queue import Queue
from StringIO import StringIO
from cookielib import CookieJar
from simple_fetcher.base import _default_lock
from threading import Thread
from urllib2 import HTTPError, URLError
import urllib2


class DownloadWorker(Thread):
    def __init__(self, lock, queue, callback=None):
        super(DownloadWorker, self).__init__()
        self.lock = lock
        self.queue = queue
        self.callback = callback
    
    def run(self):
        super(DownloadWorker, self).run()
        
        while True:
            url = self.queue.get()
            if not url:
                continue
            
            # Fetch the URL Resource
            cj = CookieJar()
            opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
            opener.addheaders = [
                ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22'),
            ]
            
            try:
                # Fetch the resource on the Internet
                response = opener.open(url).read()
                
            except HTTPError as e:
                continue
            except URLError as e:
                continue
            
            if self.callback:
                self.callback(url, response)
                
            
            self.queue.task_done();
            with self.lock: 
                if not self.queue.qsize():
                    break
            
            

class ImageDownloader(object):
    
    def __init__(self, lock=_default_lock, queue=Queue(), max=10, callback=None):
        super(ImageDownloader, self).__init__()
        
        self.lock = lock
        self.queue = queue
        self.workers = []
        self.max = max
        if callback:
            self.callback = callback
        self.images = []
    
    def add_url(self, url):
        self.queue.put(url)
        
    def clean_threads(self):
        dead_workers = []
        for worker in self.workers:
            
            if not worker.isAlive():
                dead_workers.append(worker)
                
        for worker in dead_workers:
            self.workers.remove(worker)
            del worker
        del dead_workers
        
    def download(self):
        self.clean_threads()
        
        maximum = self.max
        with self.lock:
            maximum = min(self.max, self.queue.qsize())
            if maximum < len(self.workers):
                return 
        
        for i in range(maximum):
            t = DownloadWorker(self.lock, self.queue, self.callback)
            t.setDaemon(True)
            t.start()
            self.workers.append(t)
        
            
    def join(self):
        """
        @return (list): a list of PIL Images
        """
        self.clean_threads()
        for worker in self.workers:
            worker.join()
        self.clean_threads()
        return self.images
        
    def callback(self, url, response):
        f = StringIO(response)
        image = Image.open(f)
        if url and image:
            self.images.append((url, image))

        
        
            
        
    
    
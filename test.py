from cookielib import CookieJar
from hacked_http import urllib2
import webbrowser

url = "http://sinfulcartoons.com/out.php?http://illegalanime.com" # <== put the restricted url 

cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
response = opener.open(url)

with open('test.html', 'wt') as f:
    f.write(response.read())

webbrowser.open("test.html")
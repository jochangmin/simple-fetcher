# -*- coding:utf-8 -*-
'''
Created on 2014. 6. 15.

@author: a141890
'''
from pprint import pprint
from simple_fetcher.base import SimpleFetcher
from simple_fetcher.default import DefaultParser
from simple_fetcher.naver_blog import NaverBlogParser
import unittest

class TestFetcher(SimpleFetcher):
    default_parser = DefaultParser
    parsers = [NaverBlogParser]
    
    def callback(self, url, data, parser):
        cleaned_data = data.cleaned_data
        
        print 'parser', parser
        print 'CALLBACK desc:', cleaned_data['desc']
        print 'CALLBACK images:', cleaned_data['images']
        print 'CALLBACK title:', cleaned_data['title']
        #print 'CALLBACK images:', cleaned_data['images']


class SimpleFetcherTest(unittest.TestCase):

    def test_normal(self):
        
        def callback(url, data, parser):
            cleaned_data = data.cleaned_data
        
            print 'parser', parser
            print 'CALLBACK desc:', cleaned_data['desc']
            print 'CALLBACK images:', cleaned_data['images']
            print 'CALLBACK title:', cleaned_data['title']
            #print 'CALLBACK images:', cleaned_data['images']
        
        fetcher = TestFetcher()
        fetcher.add_url("http://m.blog.naver.com/jhj0570/220036828646")
        fetcher.listen_callback(callback)
        fetcher.fetch()
        
        
        
    
if __name__ == "__main__":
    unittest.main()

    
    

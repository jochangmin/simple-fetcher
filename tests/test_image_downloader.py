'''
Created on 2014. 6. 20.

@author: a141890
'''
from pprint import pprint
from simple_downloader import ImageDownloader
from simple_fetcher.base import SimpleFetcher
from simple_fetcher.naver_blog import NaverBlogParser
import unittest


class SimpleImageDownloaderTest(unittest.TestCase):
    
    def test_download_async(self):
        return;
        downloader = ImageDownloader()
        downloader.add_url('http://naturewallpaperdesktop.com/wp-content/uploads/2014/03/nature_is_awesome_wallpapers_iphone.jpg')
        downloader.add_url('http://hdwallphotos.com/wp-content/uploads/2014/02/Awesome-River-Nature-Desktop-Wallpaper.jpg')
        downloader.download()
        images = downloader.join()
        for url,image in images:
            self.assertNotEqual(None, url)
            self.assertNotEqual(None, image)
            print url, image
    
    def test_with_naverblog_fetcher(self):
        
        parser = NaverBlogParser()
        
        fetcher = SimpleFetcher()
        fetcher.listen_callback(self.callback)
        fetcher.add_parser(parser)
        fetcher.add_url("http://blog.naver.com/hyuney0913/20208528760")
        fetcher.fetch()
    
    def callback(self, url, data, parser):
            print 'xxxxxxxxxxxx', self
            downloader = ImageDownloader()
            
            image_urls = data.cleaned_data['images']
            for image_url in image_urls:
                downloader.add_url(image_url)
            
            downloader.download()
            images = downloader.join()
            for url, image in images:
                print url, image
            
            self.assertNotEqual(None, images)
        
        
        
    
if __name__ == "__main__":
    unittest.main()

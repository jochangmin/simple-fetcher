# -*- coding:utf-8 -*-
'''
Created on 2014. 6. 15.

@author: a141890
'''
from pprint import pprint
from simple_fetcher.base import SimpleFetcher
from simple_fetcher.default import DefaultParser
from simple_fetcher.naver_blog import NaverBlogParser
import unittest
from serial.rfc2217 import EC

class TestFetcher(SimpleFetcher):
    default_parser = DefaultParser
    parsers = [NaverBlogParser]
    
    def callback(self, url, data, parser):
        cleaned_data = data.cleaned_data
        
        print 'CALLBACK title:', cleaned_data['title']
        #print 'CALLBACK images:', cleaned_data['images']


class SimpleFetcherTest(unittest.TestCase):
    
    def test_async(self):
        test = self
        
        class TestFetcher(SimpleFetcher):
            def callback(self, url, data, parser):
                cleaned_data = data.cleaned_data
                print 'CALLBACK title:', cleaned_data['title']
                test.assertNotEqual(None, url)
                test.assertNotEqual(None, cleaned_data)
                test.assertNotEqual(None, cleaned_data['title'])
                test.assertNotEqual(None, cleaned_data['content'])
                
        parser = NaverBlogParser()

        fetcher = TestFetcher()
        fetcher.add_parser(parser)
        fetcher.add_url("http://blog.naver.com/mongsil0010/220031881711")
        fetcher.add_url("http://blog.naver.com/ricky24/220031081302")
        fetcher.add_url("http://blog.naver.com/pretty758/220032150684")
        fetcher.add_url("http://jeiphia.blog.me/220031337313")
        fetcher.fetch(async=True)
        fetcher.join()
        
    def test_normal(self):
        fetcher = TestFetcher()
        fetcher.add_url("http://blog.naver.com/hyuney0913/20208528760")
        fetcher.add_url("http://blog.naver.com/wltn120/220028075376")
        fetcher.add_url("http://blog.naver.com/milyung86/10190706947")
        fetcher.add_url("http://blog.naver.com/bn6112/220031832406")
        fetcher.fetch()
        
        
        
    
if __name__ == "__main__":
    unittest.main()

    
    

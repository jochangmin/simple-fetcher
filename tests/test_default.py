'''
Created on 2014. 6. 18.

@author: a141890
'''
from pprint import pprint
from simple_fetcher.base import SimpleFetcher
from simple_fetcher.default import DefaultParser
from simple_fetcher.naver_blog import NaverBlogParser
import unittest



class _TestFetcher(SimpleFetcher):
    
    def callback(self, url, data, parser):
        # print data.cleaned_data['title']
        print parser
        pprint(data.cleaned_data)
        #print url, data


class DefaultParserTest(unittest.TestCase):
    
    def test_default(self):
        return
        test = self
        
        def callback(url, data, parser):
            print 'REGISTERED CALLBACK:', data.cleaned_data['desc']
            print 'REGISTERED CALLBACK:', data.cleaned_data['title']
        
        default_parser = DefaultParser()
        naver_parser = NaverBlogParser()
        fetcher = _TestFetcher()
        fetcher.listen_callback(callback)
        fetcher.add_parser(default_parser, default=True)
        fetcher.add_parser(naver_parser)
        
        fetcher.add_url('http://prettyzumma.blog.me/220028867296')
        fetcher.add_url('http://www.nytimes.com/2014/06/15/world/asia/hongeo-south-koreas-smelliest-food.html?ref=asia')
        fetcher.add_url('http://www.naver.com/')
        fetcher.add_url('http://www.youtube.com/watch?v=OrTyD7rjBpw&list=PL5B408043A1CBA476&index=2')
        fetcher.fetch(async=True)
        fetcher.join()
        
    def test_daum_blog(self):
        
        default_parser = DefaultParser()
        naver_parser = NaverBlogParser()
        
        fetcher = _TestFetcher()
        fetcher.add_parser(default_parser, default=True)
        fetcher.add_parser(naver_parser)
        fetcher.add_url('http://blog.daum.net/01195077236/2054?t__nil_best=leftimg')
        fetcher.fetch()
        
        
        
        
        
if __name__ == '__main__':
    unittest.main()
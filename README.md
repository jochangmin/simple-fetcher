# README #
주의!!!!!
이 라이브러리는 너무 편해서.. 
게으른 사람은 더더욱 게을러 질수 있습니다!!

단순한 웹 리소스를 가져오는데 아직도 urllib2 를 사용하신다면
Simple Fetcher를 사용하는것을 권해드립니다.

Simple Fetcher는 파이썬의 쉬운 fetch기능을 (urllib2, urllib, httplib 등등)
더더더더욱 쉽게 만들어 드립니다.

 - 쉬운 쓰레드 사용
 - 미리 만들어진 파서들 (네이버블로그, 유튜브, 뉴욕타임스 등등 계속 등록중)
 - 차단 필터 통과 기능 ([Hacked Http](https://bitbucket.org/jochangmin/hacked-urllib2) 참고)
 - 코드가 간결해짐. 라이브러리라기 보다는 작은 프레임워크에 가까움 (Customization시)
 - 그냥 사용이 쉬움. 


# Why #
왜? 사용해야하는가.
예를 들어서 Naver Blog의 경우 urllib2로 불러올경우 내용이 하나도 보이지가 않습니다.
이유는 frame 태그를 사용해서 실제 블로그 내용을 감추기 때문이죠. 
따라서.. 실제 블로그 내용을 가져오려면 다시한번 url resource를 긁어와야 됩니다.

다시말하면.. Naver Blog의 경우 최소 urllib2를 2번이나 불러야 합니다. 

Simple Fetcher는 이러한 실제 본 내용에 도달하는데 까지 모든 과정을 쉽게 해결해 드립니다.


# 게다가 #
게다가.. Simple Fetcher는 유명한 웹싸이트들의 이미 미리 만들어진 소스들이 있습니다.
더이상 웹을 분석하고 가져오려고 노력하지 마세요. 

미리 만들어진 소스들을 사용해서 간단하게 원하는 데이터를 긁어오세요.

*Simple Fetcher는 Crawler 가 아닙니다.


### 요약 ###

* URL Resource 가져오기를 더더더더욱 쉽게
* 미리 만들어진 특정 싸이트 소스를 사용해서, 분석핅요없이 그냥 사용하면 됨
* Async 또는 그냥 Sync 버젼 둘다 사용 가능함
* 프로그래밍 구조적으로 산뜻하고 이쁜 코딩이 가능
* 게으른 사람들을 더더욱 게으르게 만드는 라이브러리

### 미리만들어진 파서 Parser ###
* Naver Blog Parser
* Naver Blog Parser for Mobile
* Default Parser (for everything)


계속 추가해 나갈 생각입니다.
이메일로 요청시 해당 웹싸이트에 대한 Fetcher를 만들 생각입니다.
또는 제가 필요하거나. 

### 스크린샷 ###


![screen_shot.png](https://bitbucket.org/repo/bAR6yB/images/3198961381-screen_shot.png)



### How do I get set up? ###


```
#!python

python setup.py install
```


### Naver Blog 예제 ###


```
#!python

from simple_fetcher.naver_blog import NaverblogBaseParser
from simple_fetcher.base import SimpleFecther
class NaverblogTestParser(NaverblogBaseParser):
    def callback(self, url, data):
        cleaned_data = data.cleaned_data
        print 'CALLBACK title:', cleaned_data['title']
        print 'CALLBACK images:', cleaned_data['images']
        #print 'CALLBACK content:', cleaned_data['content']

parser = NaverblogTestParser('naver_blog')

fetcher = SimpleFecther()
fetcher.add_parser(parser)
fetcher.add_url("http://blog.naver.com/hyuney0913/20208528760")
fetcher.add_url("http://blog.naver.com/wltn120/220028075376")
fetcher.add_url("http://blog.naver.com/milyung86/10190706947")
fetcher.add_url("http://blog.naver.com/bn6112/220031832406")
fetcher.fetch() 

```

### 쓰레드로 (Asynchronous) 돌리는 방법 ###

그냥 아래 두줄만 추가시키면.. 기본적으로 최고 10개의 쓰레드가 돌아가게 됩니다.
쓰레드의 숫자는 customization이 가능하며 자동으로 늘어났다가 자동으로 죽습니다.

모든것이 다 심플 에즈 헬!

```
#!python
fetcher.fetch(async=True)
fetcher.join()

```


### 그외의 싸이트나 웹페이는 어떻하지? ###

일단 네이버블로그 지원되고, 추후에 유튜브, 뉴욕타임스 같은 유명한 웹싸이트들이 기본 파서로 제공이 될 예정입니다.
하지만 그외의 웹싸이트들은 집접 만들어야 되나?

아닙니다. 그외의 기본적으로 모든 웹싸이트들을 잡아내는 파서도 지원이 되고 있습니다.

기본적으로 다음과 같은 데이터형태를 받을수 있습니다.

```
#!python

class DefaultData(SimpleData):
    url = Field()
    title = Field()
    desc = Field()
    image = Field()
```

사용방법은 add_parser 메소드 호출시에..
default=True 로 해주면 됩니다.

즉.. SimpleFetcher 는 여러개의 파서들을 등록후 사용이 가능한데..
모든 파서들이 웹페이지를 파싱하는데 실패하면.. 
기본적으로 제공하는 파서.. 즉 default parser가 실행이 됩니다.
```
#!python

default_parser = DefaultParser()

fetcher = SimpleFetcher()
fetcher.add_parser(default_parser, default=True)
fetcher.add_url('http://prettyzumma.blog.me/220028867296')
fetcher.add_url('http://www.nytimes.com/2014/06/15/world/asia/hongeo-south-koreas-smelliest-food.html?ref=asia')
fetcher.add_url('http://www.naver.com/')
fetcher.add_url('http://www.youtube.com/watch?v=OrTyD7rjBpw&list=PL5B408043A1CBA476&index=2')
fetcher.fetch()
```

### 긁어온 데이터를 받는 방법 ###

긁어와진 데이터는 SimpleData 의 instance로 받게되는데 2가지 종류로 callback 함수를 실행할수 있습니다.
첫번째는 파서를 subclass할때 callback 함수를 재정의 하는 것입니다.

```
#!python

class NaverblogTestParser(NaverblogBaseParser):
    def callback(self, url, data):
        cleaned_data = data.cleaned_data
        print 'CALLBACK', cleaned_data
```

또는 SimpleFetcher 에 callback을 등록시키는 것입니다.


```
#!python

def callback(parser, url, data):
    print 'CALLBACK', data.cleaned_data

fetcher = SimpleFetcher()
fetcher.listen_callback(callback)
```

차이점은 Parser에 있는 callback을 사용시.. 각각의 parser에 있는 callback이 해당하는 데이터를 처리하게 되고..
Fetcher에 callback을 등록시 parser에 있는 callback은 무시되고 Fetcher에 등록된 callback만 실행이 됩니다.

Parser의 callback을 override하여 사용하는 것을 권합니다.



### Who do I talk to? ###

파이썬/안드로이드/빅데이터/서버개발/Collective Intelligence 개발자  조창민입니다.

a141890@gmail.com
으로 연락주세요
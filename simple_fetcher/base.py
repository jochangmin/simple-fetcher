# -*- coding:utf-8 -*-
'''
Created on 2014. 6. 15.

@author: a141890
'''
from Queue import Queue
from cookielib import CookieJar
from threading import Thread, Lock
from urllib2 import HTTPError, URLError
import codecs
import os
import re
import urllib2
import webbrowser

_default_lock = Lock()

class _ParserManager(object):
    def __init__(self):
        self.parsers = {}
        self.default_parser = None
    
    def add_parser(self, fetcher, parser, default=False):
        parser._set_fetcher(fetcher)
        if default:
            self.default_parser = parser
        else:
            self.parsers[parser.key] = parser
    
    def get_all_parsers(self):
        response = []
        response += self.parsers.values()
        if self.default_parser:
            response.append(self.default_parser)
        return response
    
    def remove_parser(self, parser):
        if parser.key in self.parsers:
            del self.parsers[parser.key]
    
        
class SimpleFetchWorker(Thread):
    
    def __init__(self, lock, queue, parser_manager, callback=None):
        Thread.__init__(self)
        self.lock = lock
        self.queue = queue
        self.parser_manager = parser_manager
        self.callback = callback
    
    def add_url(self, url):
        if url not in self.queue.queue:
            self.queue.put(url)    
    

    
    def run(self):
        super(SimpleFetchWorker, self).run()
        
        while True:
            # Get the url
            url = None
            with self.lock: 
                if not self.queue.qsize():
                    break
                url = self.queue.get()
        
            if not url:
                continue
            
            
            # Get a Parser
            # parser = self.get_parser(url)
            for parser in self.parser_manager.get_all_parsers():
                
                # Fetch the URL Resource
                cj = CookieJar()
                opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
                opener.addheaders = [
                    ('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22'),
                ]
                
                try:
                    # Fetch the resource on the Internet
                    response = opener.open(url).read()
                    response = self._utf8(response)
                    
                except HTTPError as e:
                    continue
                except URLError as e:
                    continue
                
                # Parse
                data = None
                parse_names = [k for k in dir(parser.__class__) if k.startswith('parse')]
                parse_names.sort()
                stop_parse = False
                for parse_name in parse_names:
                    try:
                        data = getattr(parser, parse_name)(url, response)
                        if data:
                            break
                    except StopParse as e:
                        stop_parse = True
                        break
                    except Exception as e:
                        # print e, url, parse_name
                        continue
                
                
                # Check the data
                if not data or not isinstance(data, SimpleData):
                    continue
                
                if self.callback:
                    self.callback(url, data, parser)
                
                if data:
                    # Break parsing the response with other parsers
                    break
            
        return None
    
    
    
    def _utf8(self, content):
        codec_names = ['UTF-8', 'EUC-KR', 'UHC']
        for name in codec_names:
            try:
                content = content.decode(name)
                content = unicode(content)
            except Exception as e:
                # print 'SimpleParser Encoding Error. '+ name +' ERROR: ', e
                pass
            else:
                break
        return content


class SimpleFetcher(object):
    default_parser = None
    parsers = []
    
    def __init__(self, lock=_default_lock, queue=Queue(), callback=None, max_thread=10):
        self.queue = queue
        self.lock = lock
        if callback:
            self.callback = callback
        self.workers = []
        self.parser_manager = _ParserManager()
        self.max_thread = max_thread
        
        for parser in self.__class__.parsers:
            if not isinstance(parser, SimpleParser):
                parser = parser()
            self.add_parser(parser)
            
        
        default_parser = self.__class__.default_parser
        if default_parser:
            if not isinstance(default_parser, SimpleParser):
                default_parser = default_parser()
            self.add_parser(default_parser, default=True)
            
    
    def add_url(self, url):
        if url not in self.queue.queue:
            self.queue.put(url)
            
    def add_worker(self, worker):
        if isinstance(worker, SimpleFetchWorker):
            self.worers.append(worker)
    
    def add_parser(self, parser, default=False):
        self.parser_manager.add_parser(self, parser, default)
    
    @staticmethod
    def callback(self, url, data, parser):
        pass
    
    def clean_workers(self):
        dead_workers = []
        for worker in self.workers:
            if not worker.isAlive():
                dead_workers.append(worker)
        
        for worker in dead_workers:
            self.workers.remove(worker)
            del worker
        del dead_workers
        
    def fetch(self, async=False, daemon=True):
        """
        @return None
        """
        self.clean_workers()
        if async:
            max = self.max_thread
            if max < len(self.workers):
                return;
            
            for i in range(max):
                    worker = SimpleFetchWorker(self.lock, 
                                               self.queue, 
                                               self.parser_manager, 
                                               self.callback)
                    worker.setDaemon(daemon)
                    worker.start()
                    self.workers.append(worker)
        else:
            worker = SimpleFetchWorker(self.lock, 
                           self.queue, 
                           self.parser_manager,
                           self.callback)
            worker.run()
            

    
    def join(self):
        for worker in self.workers:
            worker.join()
        
            
    def listen_callback(self, callback):
        self.callback = callback
        
    def unlisten_callback(self):
        del self.callback
        
    
    
class SimpleParser(object):
    
    def __init__(self, key):
        self.key = key
        self.fetcher = None
    
    def add_url(self, url):
        if self.fetcher:
            self.fetcher.add_url(url)
            
    def web(self, response):
        if not os.path.exists("./temp"):
            os.mkdir("./temp")
            
        with codecs.open('./temp/temp.html', 'w', encoding="UTF-8") as f:
            f.write(response)
        
        webbrowser.open('./temp/temp.html')
    
    def _set_fetcher(self, fetcher):
        self.fetcher = fetcher
        
    def _make_temp_html(self):
        pass
    

class SimpleData(object):
    
    def __init__(self):
        self._is_cleaned = False
        self.cleaned_data = {}
        
    def clean(self):
        if not self._is_cleaned:
            for n in self.__class__.__dict__:
                clean_name = "clean_" + n
                
                if hasattr(self.__class__, clean_name):
                    field_instance = getattr(self, n)
                    value =self.cleaned_data.get(n, None)
                    
                    if isinstance(field_instance, Field) and value:
                        cleaned_data = getattr(self.__class__, clean_name)(self, value)
                    
                    if not cleaned_data:
                        raise ValueError(clean_name + " method must return a cleaned data")
                    
                    self.cleaned_data[n] = cleaned_data
            self._is_cleaned = True
                
    def __setattr__(self, name, value):
        if hasattr(self.__class__, name):
            method = getattr(self.__class__, name)
            if isinstance(method, Field):                
                # Convert Encoding to UTF-8
                if type(value) == str:
                    codec_names = ['UTF-8', 'EUC-KR', 'UHC']
                    for codec_name in codec_names:
                        try:
                            value = value.decode(codec_name)
                            value = unicode(value)
                        except Exception as e:
                            # print 'SimpleParser Encoding Error. '+ name +' ERROR: ', e
                            pass
                        else:
                            break
                self.cleaned_data[name] = value
                return;
        return object.__setattr__(self, name, value)
    
    def __getattribute__(self, *args, **kwargs):
        return object.__getattribute__(self, *args, **kwargs)
            


class Field(object):
    pass


class StopParse(Exception):
    pass

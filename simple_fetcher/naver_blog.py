#-*- coding:utf-8 -*-
'''
Created on 2014. 6. 16.

@author: a141890
'''


from bs4 import BeautifulSoup
from simple_fetcher.base import SimpleData, Field, StopParse
from simple_fetcher.frame_parser import FrameParser
import re
import urlparse
import webbrowser


class NaverData(SimpleData):
    url = Field()
    title = Field()
    desc = Field()
    images = Field()
    content = Field()
    
    def clean_title(self, title):
        title = re.sub('\s+', ' ', title).strip()
        return title
    
    def clean_content(self, content):
        content = content.strip()
        return content

class NaverBlogParser(FrameParser):
    
    def __init__(self, key='naver_blog'):
        super(NaverBlogParser, self).__init__(key)
        
    def parse_builtin_navermobile(self, url, response):
        print 'NAVER MOBILE BLOG', url
        rootEl = BeautifulSoup(response)
        titleEl = rootEl.find('meta', attrs={'property':"og:title"})
        descEl = rootEl.find('meta', attrs={'property':"og:description"})
        blogUrlEl = rootEl.find('meta', attrs={'property':'og:url'})
        
        contentEl = rootEl.find('div', attrs={'class':'post_ct'})
        imageEls = rootEl.find_all('img')
        
        image_urls = []
        for imageEl in imageEls:
            src = imageEl.attrs.get('onclick', None)
            if src and src not in image_urls:
                src = src.replace("javascript:location.href='", '')[:-2]
                image_urls.append(src)
        
        
        
        if not titleEl or not contentEl:
            return None
        
        data = NaverData()
        data.title = titleEl['content']
        data.desc = BeautifulSoup(descEl['content']).text
        data.content = unicode(contentEl)
        data.images = image_urls
        data.url = blogUrlEl['content']
        data.clean()
        
        return data
    
    def parse_builtin_naverblog(self, url, response):
        print 'NAVER BLOG', url
        rootEl = BeautifulSoup(response)
        titleEl = rootEl.find('meta', attrs={'property':"og:title"})
        descEl = rootEl.find('meta', attrs={'property':"og:description"})
        blogUrlEl = rootEl.find('meta', attrs={'property':'og:url'})
        contentEl = rootEl.find('div', attrs={'id':'postListBody'})
        imageEls = rootEl.find_all('img')
        
        image_urls = []
        for imageEl in imageEls:
            src = imageEl.attrs.get('src', None)
            if src and re.match('http://postfiles\d*\.naver\.net', src):
                image_urls.append(src)
                
        image_urls = list(set(image_urls))
        
        if not titleEl or not contentEl:
            return None
         
        data = NaverData()
        data.title = titleEl['content']
        data.desc = descEl['content']
        data.content = unicode(contentEl)
        data.images = image_urls
        data.url = blogUrlEl['content']
        data.clean()
        
        return data
        


    
    
    
'''
Created on 2014. 6. 18.

@author: a141890
'''
from bs4 import BeautifulSoup
from simple_fetcher.base import SimpleParser, StopParse
import re
import urlparse

class FrameParser(SimpleParser):
    
    def __init__(self, key='naver_blog'):
        super(FrameParser, self).__init__(key)
    
    def parse_builtin_frame(self, url, response):
        print 'FrameParser', url
        """
        Find all frame tags and add the url found in the frame tag. 
        We can go into the deep inside frame tag. 
        """
        bs = BeautifulSoup(response)
        redirect = None
        for frameEl in bs.find_all(u'frame'):
            main_url = frameEl.attrs.get('src', None)
            if main_url:
                redirect = urlparse.urljoin(url, main_url)
            if not re.search(r'hidden', redirect, flags=re.IGNORECASE|re.UNICODE):
                self.add_url(redirect)
        if redirect:
            raise StopParse()
        return None
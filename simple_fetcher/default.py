# -*- coding:utf-8 -*-
'''
Created on 2014. 6. 18.

@author: a141890
'''
from PIL import Image
from StringIO import StringIO
from bs4 import BeautifulSoup
from simple_fetcher.base import SimpleData, Field
from simple_fetcher.frame_parser import FrameParser
import re


class DefaultData(SimpleData):
    url = Field()
    title = Field()
    desc = Field()
    # image = Field()
    images = Field()
    
    def clean_url(self, url):
        url = re.sub('\s+', '', url)
        return url
    
    def clean_title(self, title):
        title = re.sub('\s+', ' ', title)
        return title
    
    def clean_desc(self, desc):
        desc = BeautifulSoup(desc).text
        desc = desc.strip()
        return desc
    
#     def clean_image(self, image):
#         image = re.sub('\s+', '', image)
#         return image
    
    
    
class OpenGraphParser(FrameParser):
    def parse_builtin_default(self, url, response):
        # Facebook Open Graph
        rootEl = BeautifulSoup(response)
        urlEl = rootEl.find('meta', attrs={'property':'og:url'})
        titleEl = rootEl.find('meta', attrs={'property':"og:title"})
        descEl = rootEl.find('meta', attrs={'property':'og:description'})
        imageEl = rootEl.find('meta', attrs={'property':'og:image'})
        imageEls = rootEl.find_all('img')
        
         
        # Twitter Meta Tags
        if not urlEl:
            urlEl = rootEl.find('meta', attrs={'property':'twitter:url'})
         
        if not titleEl:
            titleEl = rootEl.find('meta', attrs={'property':"twitter:title"})
         
        if not descEl:
            descEl = rootEl.find('meta', attrs={'property':'twitter:description'})
         
        if not imageEl:
            imageEl = rootEl.find('meta', attrs={'property':'twitter:image'})
            
        if not imageEl:
            image_regex = re.compile(r'(content|thumbnail|image|title)', flags=re.IGNORECASE)
            imageEl = rootEl.find('img', attrs={'class': image_regex})
            
        if not imageEl:
            imageEl = rootEl.find('img')
        
        images = []
        for el in imageEls:
            src = el.get('src', None)
            if src:
                images.append(src)
            
        if not urlEl or not titleEl or not descEl:
            return None
        
        data = DefaultData()
        data.url = urlEl['content']
        data.title = titleEl['content']
        data.desc = descEl['content']
        # data.image = imageEl['content']
        data.images = images
        data.clean()
        return data
        
            
    def callback(self, url, data):
        pass

class DefaultParser(OpenGraphParser):
    
    def __init__(self, key='default'):
        super(DefaultParser, self).__init__(key)
    
    def parse_builtin_default(self, url, response):
        data = super(DefaultParser, self).parse_builtin_default(url, response)
        if data:
            return data
        rootEl = BeautifulSoup(response)
        urlEl = rootEl.find('meta', attrs={'property':'og:url'})
        titleEl = rootEl.find('meta', attrs={'property':"og:title"})
        descEl = rootEl.find('meta', attrs={'property':'og:description'})
        # imageEl = rootEl.find('meta', attrs={'property':'og:image'})
        imageEls = rootEl.find_all('img')
        
        
        if not titleEl:
            titleEl = rootEl.find('title')
        
        if not titleEl:
            titleEl = rootEl.find('meta', attrs={'name': 'title'})
        
        
        if not titleEl:    
            for i in range(1, 3):
                htags = rootEl.find_all('h' + str(i))
                for htag in htags:
                    attr = htag.get('id', '') + ' '.join(htag.get('class', ''))
                    if re.search('(title|head)', attr):
                        titleEl = htag
                        break
                
                if not titleEl and htags:
                    for htag in htags:
                        if htag.text.strip():
                            titleEl = htag
                    
                if titleEl:
                    break
        
        if not descEl:
            descEl = rootEl.find('meta', attrs={'name': 'description'})
            
#         if not imageEl:
#             image_regex =re.compile(r'(content|thumbnail|image|title)', flags=re.IGNORECASE)
#             imageEl = rootEl.find('img', attrs={'class': image_regex})
#             
#         if not imageEl:
#             bodyEl =rootEl.find('body')
#             if bodyEl:
#                 imageEl = bodyEl.find('img')
        
        images = []
        for el in imageEls:
            src = el.get('src', None)
            if src:
                images.append(src)
        
        if not titleEl:
            return None
        
        data_url = urlEl.text if urlEl else url
        data_url = re.sub(r'\s', '', data_url)
        data_title = re.sub(r'\s+', ' ', titleEl.text).strip()
        data_desc = descEl.text if descEl else ''
        data_desc = re.sub(r'\s+', ' ', data_desc).strip()
        #data_image = imageEl.get('src', '')
        #data_image = re.sub(r'\s+', '', data_image)
        
        data = DefaultData()
        data.url = data_url
        data.title = data_title
        data.desc = data_desc
        #data.image = data_image
        data.images = images
        data.clean()
        return data
    
